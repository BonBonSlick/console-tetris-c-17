#pragma once

#include <stdio.h>
#include <Windows.h>
#include <random>

// ... Args accept as many  as need different type argument , args... use them
template<typename... Args> void printDebugInfo(const char* msg, Args... args)
{
	char msgbuf[256] = { 0 };
	// TODO - check if sprintf can have such argument type
	sprintf_s(msgbuf, msg, args...);
	OutputDebugStringA(msgbuf);
}

/*
validate data set by filling it
if we gonna use data set, better be safe and sure it is valid
*/
template<typename T> void fillArrayBuffer(T& data)
{
	// reinterpret_cast - cast type without validations
	const auto  castedData = reinterpret_cast<char*>(data);
	const auto  en = castedData + sizeof(data);
	std::fill(castedData, en, 0);
}


// namespaced methods, used to generate random numbers
namespace Impl
{
	inline unsigned int GetRandomSeed()
	{
		static std::random_device rd;
		return rd();
	}
	inline std::mt19937 gen{ GetRandomSeed() };

	template <typename T>
	using distribution = std::conditional_t<
		std::is_integral_v<T>,
		std::uniform_int_distribution<T>,
		std::uniform_real_distribution<T>>;
}

/*
======================================
Get a random number
======================================
*/
template <typename T>
inline T Random()
{
	return Impl::distribution<T>{}(Impl::gen);
}

/*
======================================
Get a random int between to integers

Parameters:
>> pA: First number
>> pB: Second number
======================================
*/
template <typename T, typename T_2>
inline T Random(const T& a, const T_2& b)
{
	return Impl::distribution<T>{ a, b }(Impl::gen);
}