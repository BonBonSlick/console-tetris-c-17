#include "TetrisGame.h"

/*
#include <algorithm>
#include <time.h>
#include <conio.h>
#include <assert.h>
#include <strsafe.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "MathHelper.h"
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <vector>
#include <string>

using namespace std;

#define WIDTH 70
#define HEIGHT 35

HANDLE wHnd;
HANDLE rHnd;
*/
void main()
{

	Pieces pieces;
	// pas pieces by reference without copositionYing data
	Board board(&pieces);

	TetrisGame app(&board, &pieces);
	app.Run();
}
