#pragma once
#include "BaseApp.h"
#include "Pieces.h"

// where game pieces drop X(columns)
inline const short int  PLAY_AREA_WIDTH = 16;
// where game pieces drop Y(rows)
inline const short int  PLAY_AREA_HEIGHT = 15;


class Board
{
private:
	/*
	POS_FREE = free position of the board, piece can move if position is free
	POS_FILLED = filled position of the board, piece will collapse hitting filled position
	*/
	enum boardIndexStatuses { POS_FREE, POS_FILLED };
	//const int POS_FREE = 0;
	//const int POS_FILLED = 1;
	int playArea[PLAY_AREA_WIDTH][PLAY_AREA_HEIGHT]; // Board that contains the pieces
	// object, arrays, collections etc used with pointers or references
	Pieces *pieces;

	void initBoard();

	/*
	removes line when filled by Y axis on play area
	*/
	void deleteLine(int positionY);
public:
	Board(Pieces *pPieces);
	~Board();

	bool isBlockFree(int positionX, int positionY);
	bool isMovementPossible(const short int positionX, const short  int positionY, const short  int pPiece, const short  int pRotation);
	void storePiece(const short int positionX, const short  int positionY, const short  int pPiece, const short int pRotation);
	int deletePossibleLines();
	bool isGameOver();
};

