#pragma once
#include <windows.h>

class CStopwatch
{
public:
	CStopwatch();
	void Start();
	//Small Step = 1 Game Tick
	void synchronizeFrameTicks(int& deltaTime, const int deltaTimeLock);
	int Now();
	int Time();
	// how many time passed since last frame 
	int milesecondsPassed = 0;
	// how many frames passed during  milesecondsPassed
	int framesPassed = 0;
private:
	LARGE_INTEGER m_liPerfFreq;
	LARGE_INTEGER m_liPerfStart;
};

// if constant defined, load class implementation
#ifdef MY_PERFORMENCE_COUNTER

// constructor
CStopwatch::CStopwatch()
{
	QueryPerformanceFrequency(&m_liPerfFreq);
	Start();
}
void CStopwatch::synchronizeFrameTicks(int& deltaTime, const int deltaTimeLock)
{
	/*
	FPS lock
	could be replaced with std::this_thread::sleep_for(deltaTimeLock)
	this_thread is from C++ 11 standart while Sleep is from WindowsApi, which works only on windows
	*/
	while (1)
	{
		// CStopwatch::Now() same  as Now(), depends on Coding Standart or short "CS" which team use
		deltaTime = CStopwatch::Now();
		if (deltaTimeLock < deltaTime) {
			break;
		}
	}

	// display in FPS & clear old values
	milesecondsPassed += deltaTime;
	framesPassed++;
	if (milesecondsPassed >= 1000)
	{
		TCHAR  szbuff[255];
		StringCchPrintf(szbuff, 255, TEXT("FPS: %d"), framesPassed);
		SetConsoleTitle(szbuff);

		framesPassed = 0;
		milesecondsPassed = 0;
	}
}

void CStopwatch::Start()
{
	QueryPerformanceCounter(&m_liPerfStart);
}

int CStopwatch::Now()
{
	// return mileseconds after start
	LARGE_INTEGER liPerfNow;
	QueryPerformanceCounter(&liPerfNow);
	/*
	 important note type casting is not safe for rounding numbers, for rounding std::round is better
	 bacause type casting 5.99 to int will return 5
	*/
	return static_cast<int>(
		(
		(liPerfNow.QuadPart - m_liPerfStart.QuadPart) * 1000
			) / m_liPerfFreq.QuadPart
		);
}

int CStopwatch::Time()
{
	LARGE_INTEGER liPerfNow;
	QueryPerformanceCounter(&liPerfNow);
	return static_cast<int>(
		(liPerfNow.QuadPart * 1000) / m_liPerfFreq.QuadPart
		);
}

#endif
