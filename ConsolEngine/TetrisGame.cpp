﻿#include "TetrisGame.h"
#include "ConsoleHelper.h"
#include "MathHelper.h"
#include "TemplateHelpers.h"
#include "Pieces.h"

#include <ctime>
#include <iostream>
#include <string>
/*
 Parent == BaseApp
 assign default values
 variables like pieces which has no state (class without variables), can be used without initialization eg pieces(pPieces)
 while board(mBoard) which has state (buffer) must be initialized
*/
TetrisGame::TetrisGame(Board *mBoard, Pieces *pPieces) : Parent(WINDOW_WIDTH, WINDOW_HEIGHT), board(mBoard)
{
	// mileseconds between frames
	deltaTimeLock = 55;

	// override parent member
	backgroundSymbol = TEXT('.');
	initGame();
	drawBoard();
}
void TetrisGame::initGame()
{
	// First piece
	pieceIndex = Random(0, Pieces::TOTAL_ELEMENTS);
	pieceIndex = Random(0, Pieces::TOTAL_ELEMENTS);
	rotationIndex = Random(0, Pieces::TOTAL_ELEMENT_ROTATIONS);
	currentPieceX = (PLAY_AREA_WIDTH / 2) + pieces->getXInitialPosition(pieceIndex, rotationIndex);
	currentPieceY = pieces->getYInitialPosition(pieceIndex, rotationIndex);

	/*printDebugInfo("pieceIndex %d \n", pieceIndex);
	printDebugInfo("rotationIndex %d \n", rotationIndex);*/

	//  Next piece
	mNextPiece = Random(0, Pieces::TOTAL_ELEMENTS);
	mNextRotation = Random(0, Pieces::TOTAL_ELEMENT_ROTATIONS);

	/*
	display next piece out of play area
	make positions resizeable, in case height or width is going to change
	*/
	int leftPaddingFromPlayAreaBorder = PLAY_AREA_WIDTH / 6;
	mNextPosX = PLAY_AREA_WIDTH + leftPaddingFromPlayAreaBorder;
	mNextPosY = PLAY_AREA_HEIGHT / 2 - (BORDER_WIDTH * 2);
}

/*
======================================
Create a random piece
======================================
*/
void TetrisGame::createNewPiece()
{
	// The new piece
	pieceIndex = mNextPiece;
	rotationIndex = mNextRotation;
	currentPieceX = (PLAY_AREA_WIDTH / 2) + pieces->getXInitialPosition(pieceIndex, rotationIndex);
	currentPieceY = pieces->getYInitialPosition(pieceIndex, rotationIndex);

	//printDebugInfo("createNewPiece currentPieceY = %d \n", currentPieceY);

	// Random next piece
	mNextPiece = Random(0, Pieces::TOTAL_ELEMENTS);
	mNextRotation = Random(0, Pieces::TOTAL_ELEMENT_ROTATIONS);
}
/*
======================================
Draw piece

Parameters:

>> positionX:        Horizontal position in blocks
>> positionY:        Vertical position in blocks
>> pPiece:    Piece to draw
>> pRotation: 1 of the 4 possible rotations
======================================
*/
void TetrisGame::drawPiece(const short int positionX, const short int  positionY, const short  int pPiece, const short  int pRotation)
{
	// Travel the matrix of blocks of the piece and draw the blocks that are filled
	for (int x = 0; x < PIECE_MATRIX_SIZE; x++)
	{
		for (int y = 0; y < PIECE_MATRIX_SIZE; y++)
		{
			/*printDebugInfo("%d", pieces->getBlockType(0, 0, x, y));
			if (y == 4) {
				printDebugInfo("\n");
			}*/

			int piece = pieces->getBlockType(pPiece, pRotation, x, y);

			if (0 != piece) {
				/*
				X and Y must be inverted here to draw correctly
				it depends on what we loop first X or Y inside matrix
				it depends on how we get block type, eg pieces[piece][rotation][positionX][positionY] / [positionY][positionX]
				it depends on pieces array declaration pieces[piece][rotation][positionX][positionY] / [positionY][positionX]
				*/
				setChar(positionX + y, positionY + x, pieceSymbol);
				//setChar(positionX + x, positionY + y, pieceSymbol);
				//printDebugInfo("positionX + x = %d positionY + y = %d \n", positionX + x, positionY + y);
			}
		}

	}
}
void TetrisGame::updateTick(const float deltaTime)
{
	//printDebugInfo("deltaTime = %a \n", deltaTime);
	//printDebugInfo("currentPieceY = %d \n", currentPieceY);

	// move piece to bottom
	milesecondsSinceLastMoveDown += deltaTime;
	if (true == isCurrentPieceShouldMoveDown(deltaTime) &&
		true == board->isMovementPossible(currentPieceX, currentPieceY + 1, pieceIndex, rotationIndex)
		) {
		milesecondsSinceLastMoveDown = 0;
		currentPieceY++;
	}
	if (false == board->isMovementPossible(currentPieceX, currentPieceY + 1, pieceIndex, rotationIndex)) {
		board->storePiece(currentPieceX, currentPieceY, pieceIndex, rotationIndex);

		int totalRemovedLines = board->deletePossibleLines();
		// it is always better to wrap logical blocks, even if multiply and devide goes first, it is easier to read and maintain
		totalScores = totalScores + (totalRemovedLines * TetrisGame::SCORES_PER_LINE);

		gameSpeedInMileseconds += TetrisGame::SPEED_INCREASE_PER_LINE;

		createNewPiece();
	}
	// always do explicit comparisons when possible
	if (true == board->isGameOver())
	{
		gameState = GAME_RESTART;
	}

	// draw, update UI (user itnerface) every frame, game tick
	drawScene();
	// redraw top border
	drawBorders(PLAY_AREA_WIDTH, PLAY_AREA_HEIGHT);
}

bool TetrisGame::isCurrentPieceShouldMoveDown(const float deltaTime)
{
	bool shouldMoveDown = false;
	if (gameSpeedInMileseconds <= milesecondsSinceLastMoveDown) {
		shouldMoveDown = true;
	}
	return shouldMoveDown;
}

/*
======================================
Draw board

Draw the two lines that delimit the board
======================================
*/
void TetrisGame::drawBoard()
{
	// by default we fill whole play screen with spaces and then only play area with dots to see piece navigation
	fillBackground(PLAY_AREA_WIDTH, PLAY_AREA_HEIGHT);
	drawBorders(PLAY_AREA_WIDTH, PLAY_AREA_HEIGHT);
	drawStoredPieces();
	drawScores();
}

void TetrisGame::drawScores()
{
	int bottomPadding = 1 + BORDER_WIDTH + BUFFER_VIEW_OVERFLOW;
	int leftPadding = 2;
	std::string scoreText = "> Score: ";
	drawText(leftPadding, Y_SIZE - bottomPadding, scoreText.append(std::to_string(totalScores)));

	int borderBottomPadding = 2 + bottomPadding;
	WORD borderColor = FOREGROUND_GREEN | FOREGROUND_INTENSITY;
	for (int x = 0; x < X_SIZE; x++)
	{
		for (int y = 0; y < Y_SIZE; y++)
		{
			if (y == Y_SIZE - borderBottomPadding) {
				setChar(x, y, borderSymbol, borderColor);
			}
		}
	}
}

void TetrisGame::drawStoredPieces()
{
	for (int x = 0; x < PLAY_AREA_WIDTH; x++)
	{
		for (int y = 0; y < PLAY_AREA_HEIGHT; y++)
		{
			// Check if the block is filled, if so, draw it
			if (false == board->isBlockFree(x, y)) {
				setChar(x, y, pieceSymbol);
			}
		}
	}
}

void TetrisGame::clearOldPiecePreview()
{
	for (short int x = 0; x < PIECE_MATRIX_SIZE; x++)
	{
		for (short int y = 0; y < PIECE_MATRIX_SIZE; y++)
		{
			// TODO - refactor ba ckgroundSymbol, move it to board and TetrisGame, not BaseApp
			setChar(mNextPosX + x, mNextPosY + y, TEXT(' '));
		}
	}
}

void TetrisGame::drawText(short  int xPosition, const short  int yPosition, std::string text)
{
	int charLength = text.length();
	int totalIterationsToDrawText = xPosition + charLength;
	int stringStartIndex = 0;
	for (xPosition; xPosition < totalIterationsToDrawText; xPosition++)
	{
		setChar(xPosition, yPosition, text.at(stringStartIndex));
		stringStartIndex++;
	}
}

/*
======================================
Draw scene

Draw all the objects of the scene
======================================
*/
void TetrisGame::drawScene()
{
	// Draw the delimitation lines and blocks stored in the board
	drawBoard();

	//printDebugInfo("pieceIndex = %d rotationIndex = %d \n", pieceIndex, rotationIndex);

	// Draw the playing piece
	drawPiece(currentPieceX, currentPieceY, pieceIndex, rotationIndex);

	//printDebugInfo("mNextPiece = %d mNextRotation= %d \n", mNextPiece, mNextRotation);

	// clear old drawn piece
	clearOldPiecePreview();

	// Draw the next piece
	drawPiece(mNextPosX, mNextPosY, mNextPiece, mNextRotation);
}

void TetrisGame::KeyPressed(const   int  btnCode)
{
	/*
	check collision (with blocks & borders)
	and if piece fits (can move, be puzzled in game area with other pieces)
	before move
	*/
	MOVE_DOWN == btnCode && true == board->isMovementPossible(currentPieceX, currentPieceY + 1, pieceIndex, rotationIndex) ? currentPieceY++ : 0;
	TURN_LEFT == btnCode && true == board->isMovementPossible(currentPieceX - 1, currentPieceY, pieceIndex, rotationIndex) ? currentPieceX-- : 0;
	TURN_RIGHT == btnCode && true == board->isMovementPossible(currentPieceX + 1, currentPieceY, pieceIndex, rotationIndex) ? currentPieceX++ : 0;

	// we add 1, because 0 can not be devided in case rotation type is 0
	int futureRotation = (rotationIndex + 1) % (Pieces::TOTAL_ELEMENT_ROTATIONS + 1);
	ROTATE == btnCode && true == board->isMovementPossible(currentPieceX, currentPieceY, pieceIndex, futureRotation) ? rotationIndex = futureRotation % 4 : 0;

	/*printDebugInfo("btnCode = %d \n", btnCode);
	printDebugInfo("rotationIndex = %d \n", rotationIndex);
	printDebugInfo("rotationIndex NEW  = %d \n", (rotationIndex + 1) % 4);
	printDebugInfo("isMovementPossible  = %s \n", true == board->isMovementPossible(currentPieceX, currentPieceY, pieceIndex, rotationIndex) ? "TRUE" : "FALSE");*/

}
