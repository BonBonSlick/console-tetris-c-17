﻿// Header of current class must be included first to avoid  possible dependencies of .h file
#include "BaseApp.h"

#include <algorithm>
#include <time.h>
#include <conio.h>
#include <strsafe.h>
#include <thread>

// may cause error like redifenition during initialization, if this constant declared, load Counter class implementation. ifndef better
#define MY_PERFORMENCE_COUNTER

#include "PerformanceCounter.h"
#include "ConsoleHelper.h"
#include "TetrisGame.h"
#include "TemplateHelpers.h"

// required for this_threadto work
using namespace std::chrono_literals;

// here we initialize constants  for this scope with passed values, because these constants are class members
// we define local scope constants Y_SIZE same as  WINDOW_HEIGHT
BaseApp::BaseApp(int xSize, int ySize) : X_SIZE(xSize), Y_SIZE(ySize)
{
	// setting up console window http://cecilsunkure.blogspot.com/2011/11/windows-console-game-setting-up-window.html
	consoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	consoleInput = GetStdHandle(STD_INPUT_HANDLE);

	ConsoleHelper::preventManualWindowResize();

	// we want our play screen to be specific size, static functions are called without initiating class instance
	// important note, we want to resize only once
	ConsoleHelper::resize(consoleOutput, X_SIZE, Y_SIZE);

	// after game was initialized, we do not want user manually edit it is size,
	// even if it is windowed mode, we should allow resize for only to specific resolutions throug ingame options
	// customize font
	ConsoleHelper::setUpFontSettings();

	// hiding console cursor to avoid input with mouse
	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(consoleOutput, &cursorInfo);
	cursorInfo.bVisible = FALSE;
	cursorInfo.dwSize = 1;
	SetConsoleCursorInfo(consoleOutput, &cursorInfo);
	/*
	sizeof - returns CHAR_INFO size in bits, may differ on devices
	malloc - allocates a block of size bytes of memory, returning a pointer to the beginning of the block.
	malloc - C function, must be used with free(bufferName) to free memmory!
	TODO for C++ vector array and delete[] might be better rather than malloc()
	CHAR_INFO - Specifies a Unicode or ANSI character and its attributes
	buffer is used by console functions to read from and write to a console screen
	buffer size must be bigger then window
	never user C way of type casting in C++
	https://stackoverflow.com/questions/332030/when-should-static-cast-dynamic-cast-const-cast-and-reinterpret-cast-be-used
	*/
	consoleCharactersBuffer = static_cast<CHAR_INFO*>(malloc(X_SIZE* Y_SIZE * sizeof(CHAR_INFO)));

	// check if window buffer size and input buffer sizes are valid
	fillArrayBuffer(consoleCharactersBuffer);

	// these required to render output
	mDwBufferSize.X = X_SIZE;
	mDwBufferSize.Y = Y_SIZE;	// data buffer size  

	mDwBufferCoord.X = 0;
	mDwBufferCoord.Y = 0;				// cell coordinates

	// output area
	mLpWriteRegion.Left = 0;
	mLpWriteRegion.Top = 0;
	mLpWriteRegion.Right = X_SIZE - BUFFER_VIEW_OVERFLOW;
	mLpWriteRegion.Bottom = Y_SIZE - BUFFER_VIEW_OVERFLOW;	// rectangle  for reading

	// default game state and later it is current state
	gameState = GAME_INIT;


	// we draw borders and background only once during initialization, because we do not want draw them every game tick
	fillBackground(X_SIZE, Y_SIZE);

	// without BUFFER_VIEW_OVERFLOW border drawn out of view
	drawBorders(X_SIZE - BUFFER_VIEW_OVERFLOW, Y_SIZE - BUFFER_VIEW_OVERFLOW);

	// prints out debug info with templat function in debug Output console
	//printDebugInfo("1111111111 %d", X_SIZE);
}

BaseApp::~BaseApp()
{
	// dispose old data, free buffer
	free(consoleCharactersBuffer);
}

void BaseApp::fillBackground(const int & width, const int & height) {
	// every X axis position
	for (int x = 0; x < width; x++)
	{
		// fill background with setChar() by Y axis from top to bottom
		for (int y = 0; y < height; y++)
		{
			setChar(x, y, backgroundSymbol);
		}
	}
}

void BaseApp::drawBorders(const int & width, const int & height)
{
	WORD borderColor = FOREGROUND_GREEN | FOREGROUND_INTENSITY;
	//every X axis position
	for (int x = 0; x <= width; x++)
	{
		// draws top and bottom  borders lines
		setChar(x, 0, borderSymbol, borderColor);
		setChar(x, height, borderSymbol, borderColor);
		// draws left and right
		for (int y = 0; y < height; y++)
		{
			if (0 == x || x == width) {
				setChar(x, y, borderSymbol, borderColor);
			}
		}
	}
}

void BaseApp::setChar(int x, int y, TCHAR  c)
{
	if (false == isCharCanBeSet(x, y)) {
		return;
	}
	// getting position of new char on previous line in the end (X_SIZE)*y
	// eg if you want to find 5 th index in buffer
	// 0 1 2  X = 2
	// 3 4 5  Y = 1
	// X * y = bufferIndex[2]
	// because buffer is one demensional
	// now add position by x axis X * y + 3, and you will get value of  bufferIndex[5]
	int bufferIndex = X_SIZE * y + x;
	// replaces buffer char by index with new encoded to unicode
	consoleCharactersBuffer[bufferIndex].Char.UnicodeChar = c;
	// char settings https://docs.microsoft.com/en-us/windows/console/char-info-str
	consoleCharactersBuffer[bufferIndex].Attributes = textColor;
}

// method is overloaded, polymorphism is one of the basic OOP concepts, overloading is small way to do it
void BaseApp::setChar(int x, int y, TCHAR c, WORD characterAttributes)
{
	if (false == isCharCanBeSet(x, y)) {
		return;
	}
	int bufferIndex = x + X_SIZE * y;
	consoleCharactersBuffer[bufferIndex].Char.UnicodeChar = c;
	consoleCharactersBuffer[bufferIndex].Attributes = characterAttributes;
}

TCHAR BaseApp::GetChar(int x, int y)
{
	// UnicodeChar - wide range character, Ascii in only english
	return consoleCharactersBuffer[x + (X_SIZE)*y].Char.UnicodeChar;
}

void BaseApp::Render()
{
	// write data to console with console output buffer
	// more info http://cecilsunkure.blogspot.com/2011/11/windows-console-game-writing-to-console.html
	if (!WriteConsoleOutputW(consoleOutput, consoleCharactersBuffer, mDwBufferSize, mDwBufferCoord, &mLpWriteRegion))
	{
		// printf  is C function also it is faster and easier to read comparing to cout
		printf("WriteConsoleOutput failed - (%d)\n", GetLastError());
		exit(0);
	}
}

bool BaseApp::isCharCanBeSet(const int x, const int y)
{
	bool canBeSet = true;
	if (x == X_SIZE || y == Y_SIZE) {
		canBeSet = false;
	}

	return canBeSet;
}

void BaseApp::Run()
{
	CStopwatch timer;

	// Game main loop
	while (gameState != GAME_EXIT)
	{
		// Handle game states
		switch (gameState)
		{
		case GAME_INIT:
		{
			/*
			allocate memory
			load resources
			prepare game
			*/
			gameState = GAME_MENU;
		} break;
		// menu, start, restart, exit etc
		case GAME_MENU:
		{
			// TODO - screens eg StartScreen, PlayScreen, EndScreen
			// draw menu and handle input, eg 1 - start game, 2 - exist available  menu options
			gameState = GAME_STARTING;
		} break;
		// game is starting
		case GAME_STARTING:
		{
			// prepare level to play, like load level assets etc
			gameState = GAME_RUN;
		} break;
		// game is running
		case GAME_RUN:
		{
			// game logic, if we were using OpenGL we might want to switch screen passing BaseApp by reference, 
			// like PlayScreen to handle UI nad play logic there

			// clear the screen and redraw, case if user restarted
			//clearScreen();

			// start tracking performance
			timer.Start();

			/*
				input console events can be used http://cecilsunkure.blogspot.com/2011/11/windows-console-game-event-handling.html
				to handle inputs or write own with STL <map> or simple array with [EVENT_NAME][SUBSCRIBER_1, SUBSCR_2...]
			*/
			bool isKeyboardPressed = _kbhit();
			if (true == isKeyboardPressed)
			{
				int pressedKeyCode = _getch();

				KeyPressed(pressedKeyCode);

				// All input records currently in the input buffer are discarded.
				if (!FlushConsoleInputBuffer(consoleInput)) {
					std::cout << "FlushConsoleInputBuffer failed with error " << GetLastError();
					exit(0);
				}
			}

			// "f" after 1000 is floating number literal suffix http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2017/n4659.pdf#page=43
			updateTick(static_cast<float>(deltaTime));

			Render();

			/*
			synchronize between frames
			show performance (FPS)
			*/
			timer.synchronizeFrameTicks(deltaTime, deltaTimeLock);

			// the only way to change that gameState is by user interractions, like death, lost game, exit etc
		} break;
		case GAME_RESTART:
		{
			// TODO - set restart screen, where user can restart or exit game
				// display restart game menu
			exit(0);
			return;
		} break;
		case GAME_EXIT:
		{
			//  if we here, means no error occured,
			// close window
			return;
		} break;
		}
	}
}