#pragma once

#include <cstdlib>

class MathHelper
{
public:
	// Square = width * height
	static int calculateSquare(const int width, const int height);
};

