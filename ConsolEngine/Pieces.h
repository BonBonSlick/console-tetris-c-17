#pragma once

// Number of horizontal and vertical blocks of a matrix piece eg by X axis {0, 0, 0, 0, 0},
inline int  const PIECE_MATRIX_SIZE = 5;

class Pieces
{
public:
	int getBlockType(unsigned int pPiece, int pRotation, int positionX, int positionY);
	int getXInitialPosition(int pPiece, int pRotation);
	int getYInitialPosition(int pPiece, int pRotation);
	// From 0 to 6, total 7
	static const short int TOTAL_ELEMENTS = 6;
	// From 0 to 3, total 4
	static const short int TOTAL_ELEMENT_ROTATIONS = 3;
};

