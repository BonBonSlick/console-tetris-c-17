#include "Board.h"
#include "TemplateHelpers.h"

Board::Board(Pieces * pPieces)
{
	initBoard();
}

void Board::initBoard()
{
	// fillin play area withh indexes (POS_FREE) where piece can fit, move by X and Y axis
	for (int x = 0; x < PLAY_AREA_WIDTH; x++) {
		for (int y = 0; y < PLAY_AREA_HEIGHT; y++) {
			playArea[x][y] = POS_FREE;
			/*printDebugInfo("ARRAY X - %d and Y - %d \n", x, y);
			printDebugInfo("DATA %d \n", playArea[x][y]);
			printDebugInfo("POS_FILLED == %s \n", POS_FILLED == playArea[x][y] ? "true" : "false");*/
		}
	}
	// validate buffer
	fillArrayBuffer(playArea);
}

Board::~Board()
{
}


/*
======================================
Store a piece in the board by filling the blocks

Parameters:

>> positionX:        Horizontal position in blocks
>> positionY:        Vertical position in blocks
>> pPiece:    Piece to draw
>> pRotation: 1 of the 4 possible rotations
======================================
*/
void Board::storePiece(const short int positionX, const short int positionY, const short int pPiece, const short int pRotation)
{
	// Store each block of the piece into the board
	// by X axis
	for (int x1 = positionX, x2 = 0; x1 < positionX + PIECE_MATRIX_SIZE; x1++, x2++)
	{
		// by Y axis
		for (int y1 = positionY, y2 = 0; y1 < positionY + PIECE_MATRIX_SIZE; y1++, y2++)
		{
			// Store only the blocks of the piece that are not holes
			// -> is used to acces methods through pointers, because pieces are pointer
			if (0 == pieces->getBlockType(pPiece, pRotation, y2, x2)) {
				continue;
			}
			playArea[x1][y1] = POS_FILLED;

			//printDebugInfo("STORING x = %d  y = %d\n", x1, y1);
		}
	}
}

/*
======================================
Check if the game is over becase a piece have achived the upper position

Returns true or false
======================================
*/
bool Board::isGameOver()
{
	// declaring return value prevents unpredicted behaviour and bugs, only one return should be
	bool isGameOver = false;

	//If the first line has blocks, then, game over
	for (int i = 0; i < PLAY_AREA_WIDTH; i++)
	{
		/*
		POS_FILLED == playArea[i][0] or (staticData operator dynamicData) yoda style comparison, perevents unexpected behaviour and bugs
		top line of game filed, area
		*/
		if (POS_FILLED == playArea[i][0]) {
			isGameOver = true;
		}
	}

	return isGameOver;
}

/*
======================================
Delete a line of the board by moving all above lines down

Parameters:

>> positionY:        Vertical position in blocks of the line to delete
======================================
*/
void Board::deleteLine(int positionY)
{
	// Moves all the upper lines one row down
	// TODO - add animation, like replacing line with ======= and then remove it after sleep 1 sec

	// looping from bottom to top of play area
	for (int y = positionY; y > 0; y--)
	{
		// looping from X axis from start till the end and removing line from left to right by replacing upper piece with one below [y - 1]
		for (int x = 0; x < PLAY_AREA_WIDTH; x++)
		{
			playArea[x][y] = playArea[x][y - 1];
		}
	}
}

/*
======================================
Delete all the lines that should be removed
======================================
*/
int Board::deletePossibleLines()
{
	int totalRemovedLines = 0;
	// loopin from top to bottom
	for (int y = BORDER_WIDTH; y < PLAY_AREA_HEIGHT; y++)
	{
		int x = BORDER_WIDTH;

		// no need to continue loop if at lea st 1 position is not filled
		if (true == isBlockFree(x, y)) {
			continue;
		}

		// loop from left to right
		while (x < PLAY_AREA_WIDTH)
		{
			if (true == isBlockFree(x, y)) {
				break;
			}
			x++;
		}

		// if whole line (play area by X axis is filled from left to right) is FILLED remove it starting by Y axis
		int bothSidesBorderWidth = 2 * BORDER_WIDTH;
		if (x == PLAY_AREA_WIDTH) {
			//printDebugInfo("deleteLine = %d \n", y);
			deleteLine(y);
			totalRemovedLines++;
		}
	}

	return totalRemovedLines;
}

/*
======================================
Returns 1 (true) if the this block of the board is empty, 0 if it is filled

Parameters:

>> positionX:        Horizontal position in blocks
>> positionY:        Vertical position in blocks
======================================
*/
bool Board::isBlockFree(int positionX, int positionY)
{
	return POS_FREE == playArea[positionX][positionY];
}

/*
======================================
Check if the piece can be stored at this position without any collision
Returns true if the movement is  possible, false if it not possible

Parameters:

>> positionX:        Horizontal position in blocks
>> positionY:        Vertical position in blocks
>> pPiece:    Piece to draw
>> pRotation: 1 of the 4 possible rotations
======================================
*/
bool Board::isMovementPossible(const short int positionX, const short  int positionY, const short int pPiece, const short  int pRotation)
{
	//printDebugInfo("positionY = %d \n", positionY);

	bool isMovementPossible = true;
	/*
	 Checks collision with pieces already stored in the board or the board limits
	 This is just to check the 5x5 blocks of a piece with the appropriate area in the board
	 x1 and y1 are positions on board, x2 and y2 are matrix positions
	 tempX and Y are used to avoid method parameters positionY & X reinitialization, in result which may case in unexcepted bugs and behaviour
	*/
	int tempX = positionX;
	int tempY = positionY;
	//printDebugInfo("tempY = %d \n", tempY);
	for (int x1 = tempX, x2 = 0; x1 < tempX + PIECE_MATRIX_SIZE; x1++, x2++)
	{
		for (int y1 = tempY, y2 = 0; y1 < tempY + PIECE_MATRIX_SIZE; y1++, y2++)
		{
			//printDebugInfo("y1 = %d \n", y1);
			// no need to handle empty piece parts
			if (0 == pieces->getBlockType(pPiece, pRotation, y2, x2)) {
				continue;
			}

			// Check if the piece is outside the limits of the board
			bool isOutsideBorders = x1 <= 0 ||
				x1 >= PLAY_AREA_WIDTH ||
				y1 >= PLAY_AREA_HEIGHT;

			if (true == isOutsideBorders) {
				isMovementPossible = false;
			}

			// Check if the piece have collisioned with a block already stored in the map
			if (0 <= y1 && false == isBlockFree(x1, y1)) {
				isMovementPossible = false;
			}

			/*printDebugInfo("PLAY_AREA_WIDTH = %d PLAY_AREA_HEIGHT = %d \n", PLAY_AREA_WIDTH, PLAY_AREA_HEIGHT);
			printDebugInfo("positionX = %d  \n", positionX);
			printDebugInfo("positionY = %d  \n", positionY);
			printDebugInfo("x1 = %d y1 = %d \n", x1, y1);
			printDebugInfo("x2 = %d y1 = %d \n", x2, y2);
			printDebugInfo("pPiece = %d \n", pPiece);
			printDebugInfo("pRotation = %d \n", pRotation);
			printDebugInfo("getBlockType = %d \n", pieces->getBlockType(pPiece, pRotation, y2, x2));
			printDebugInfo("isOutsideBorders = %s \n", true == isOutsideBorders ? "true" : "false");*/
		}
	}
	//printDebugInfo("isMovementPossible %s \n", true == isMovementPossible ? "true" : "false");

	return isMovementPossible;
}

