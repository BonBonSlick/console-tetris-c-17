﻿// #pragma once - prevents include duplicates. Used in .h files. Works ONLY on  Windows! ifndef  is better for multiplatform apps.
#pragma once

#include <Windows.h>
#include <iostream>

// Usually we should using namespaced modules, because of local scope collision possibility and performance, we do not need everythings from lib. 
// std:: module modifier usually used to access lib. EG std::vector< std::string > not vector<string>
//using namespace std;

// if file included in header, it will be shared across all other files which include current
// if include .cpp file specific, eg used only there, should be declared there
//#include "ConsoleHelper.h"

/*
"inline" is used to avoid duplications, case if .h included twice const would be initialized twice
only for C++ 17 and above, below C++ 17 "extern" is used instead of "inline"
*/
// game states
// initialization
inline int  const GAME_INIT = 1;
// menu mode, screen
inline int  const GAME_MENU = 2;
// game is starting
inline int  const GAME_STARTING = 3;
// game is running
inline int  const GAME_RUN = 4;
// restarting game
inline int  const GAME_RESTART = 5;
// exit game
inline int  const GAME_EXIT = 6;
// border can be more than 1 symbol
inline int  const BORDER_WIDTH = 1;
// console is smaller than buffer, as result data can be rendered out of console view
// View area overflows Buffers, without it would be inner padding
// 1 is how much cols and rows are out of screan view area on the right and bottom window sides
inline int  const BUFFER_VIEW_OVERFLOW = 1;

class BaseApp
{
private:
	/*
	The data to be written to the console screen buffer.
	This pointer is treated as the origin of a two-dimensional array of CHAR_INFO structures
	whose size is specified by the dwBufferSize parameter.
	*/
	CHAR_INFO* consoleCharactersBuffer;
	// The size of the buffer pointed to by the "consoleCharactersBuffer" parameter, in character cells.
	COORD mDwBufferSize;
	// The X member of the COORD structure is the column, and the Y member is the row.
	COORD mDwBufferCoord;
	/*
	Cells in the destination rectangle whose corresponding source location are outside
	the boundaries of the source buffer rectangle are left unaffected by the write operation.
	mLpWriteRegion  - specifies the size and location of the block to be written to in the console screen buffer
	*/
	SMALL_RECT mLpWriteRegion;

	/*
	Flush data buffer to console, draws characters
	*/
	void Render();

	// bool means return true or false
	// should prevent filling console handler character buffer out of screen view
	bool isCharCanBeSet(const int x, const int y);

protected:
	// console window handlers, require to manipulate input and output data graphically
	HANDLE consoleOutput;
	HANDLE consoleInput;
	// void means that function will return nothing
	void fillBackground(const int & width, const int & height);
	// draws circle border around specified area
	void drawBorders(const int & width, const int & height);
public:
	//console background symbol
	// important note TCHAR differs from char, may result in hard to tracks bugs
	// TCHAR  is for multilanguage apps
	// TEXT is a mcaros to compile text as Unicode or ANSI(English), macros is compile settings dependent
	TCHAR  backgroundSymbol = TEXT(' ');
	TCHAR  borderSymbol = TEXT('#');
	TCHAR  pieceSymbol = TEXT('\u25d8');  // ◘ == \u25d8
	//TCHAR   pieceSymbol = TEXT('O');
	// single quotes ' for single character, double " for strings
	TCHAR  filledLineSymbol = TEXT('-');

	// we declare and initialize member, variable with default value avoiding setting it in constructors or else where
	int deltaTime = 0;
	// state of the game, like initializing, loading, playing, exit etc
	int gameState;

	/*
	Think about  X_SIZE and Y_SIZE as one dimensional array, where coordinates  it is position by axis.
	EG x = 0 and y 3 in array 3x3 we will get elemnt by index 7, in tic tac toe it is left bottom corner cell
	*/
	// Console Screen Size X(columns)
	const short int  X_SIZE;
	// Console Screen Size Y (rows)
	const short int  Y_SIZE;
	// max FPS
	short int deltaTimeLock;

	WORD textColor = FOREGROUND_GREEN;

	// Arguments are input area size X & Y axis
	BaseApp(int xSize = 15, int ySize = 100);

	// destructors in any class with virtual members or functions should be virtual
	// "Effective C++: 50 Specific Ways to Improve Your Programs and Design (2nd Edition)"
	virtual ~BaseApp();

	// Run the game loop
	void Run();

	// sets specific char in output area by X & Y coordinates
	void setChar(int x, int y, TCHAR  c);
	void setChar(int x, int y, TCHAR  c, WORD characterAttributes);

	// get char in output area by X & Y coordinates
	TCHAR GetChar(int x, int y);

	/*
	Called every tick
	deltaTime - is the time between frames
	can be overriten in child classes
	virtual - means abstract, aka interface method
	 = 0; it is pure virtual function, all derived not abstract classes should implement it,
	 if not, derrived class is also abstract as current one
	*/
	virtual void updateTick(float deltaTime) {};

	/*
	 What we do if window was resized, eg reutn back to specific resolution
	*/
	//virtual void isWindowWasResized() = 0;

	/*
	Handles key press
	accepts btn key code
	to get btn key code call  getch() from  <conio.h> lib
	can be overriten in child classes
	for better scaleability add event on key pressed and handle in different listeners different logic
	*/
	virtual void KeyPressed(int btnCode) {};
};