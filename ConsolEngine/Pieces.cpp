#include "Pieces.h"
#include "TemplateHelpers.h"

/* we could use complex rotation algebra in order to rotate the piece
	 or just store rotated pieces in multidemensional array
	 Pieces definition
	 2 - is rotation point
	 1 - is part of element
	 0 - free space
	 TODO - use complex tetromino vector math (good performance, hard to read) or objects (lower performance, easier to read)
	 */
inline const char pieceIndexs[7 /*kind */][4 /* rotation */][5 /* horizontal blocks */][5 /* vertical blocks */] =
{
	// Square
	  { // 0  kind of piece
	   { // 0  rotation
		{0, 0, 0, 0, 0}, // position X
		{0, 0, 0, 0, 0}, // position Y is inside array
		{0, 0, 2, 1, 0}, // 2 is rotation center, center of 5x5 array
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   {  // 2
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0},
		},
	   { // 3
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0},
		}
	   },

	// I
	  { // 1
	   { // 0
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 1, 2, 1, 1},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		},
	   { // 2
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{1, 1, 2, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		},
	   { // 3
		{0, 0, 1, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0},
		}
	   },

	// L
	  { // 2
	   { // 0
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 1, 2, 1, 0},
		{0, 1, 0, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 2
		{0, 0, 0, 0, 0},
		{0, 1, 1, 0, 0},
		{0, 0, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 3
		{0, 0, 0, 0, 0},
		{0, 0, 0, 1, 0},
		{0, 1, 2, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0}
		}
	   },

	// L mirrored
	  { // 3
	   { // 0
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 0, 0},
		{0, 1, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 1, 0, 0, 0},
		{0, 1, 2, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 2
		{0, 0, 0, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 0, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 3
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 1, 2, 1, 0},
		{0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0}
		}
	   },

	// N
	   { // 4
		{ // 0
		 {0, 0, 0, 0, 0},
		 {0, 0, 0, 1, 0},
		 {0, 0, 2, 1, 0},
		 {0, 0, 1, 0, 0},
		 {0, 0, 0, 0, 0}
		 },
		{ // 1
		 {0, 0, 0, 0, 0},
		 {0, 0, 0, 0, 0},
		 {0, 1, 2, 0, 0},
		 {0, 0, 1, 1, 0},
		 {0, 0, 0, 0, 0}
		 },
		{ // 2
		 {0, 0, 0, 0, 0},
		 {0, 0, 1, 0, 0},
		 {0, 1, 2, 0, 0},
		 {0, 1, 0, 0, 0},
		 {0, 0, 0, 0, 0}
		 },
		{ // 3
		 {0, 0, 0, 0, 0},
		 {0, 1, 1, 0, 0},
		 {0, 0, 2, 1, 0},
		 {0, 0, 0, 0, 0},
		 {0, 0, 0, 0, 0}
		 }
		},

	// N mirrored
	  { // 5
	   { // 0
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 0, 1, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 1, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 2
		{0, 0, 0, 0, 0},
		{0, 1, 0, 0, 0},
		{0, 1, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 3
		{0, 0, 0, 0, 0},
		{0, 0, 1, 1, 0},
		{0, 1, 2, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0}
		}
	   },

	// T
	  { // 6
	   { // 0
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 2, 1, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 1
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{0, 1, 2, 1, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 2
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 1, 2, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 0, 0, 0, 0}
		},
	   { // 3
		{0, 0, 0, 0, 0},
		{0, 0, 1, 0, 0},
		{0, 1, 2, 1, 0},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0}
		}
	   }
};
/*
Displacement of the piece to the position where it is first drawn in the board when it is created
should be generated outside of view
*/
inline const int pieceIndexsInitialPosition[7 /*kind */][4 /* r2otation */][2 /* position */] =
{
	/* Square */
	  { // 0
		{-2, -3},
		{-2, -3},
		{-2, -3},
		{-2, -3}
	   },
	/* I */
	  { // 1
		{-2, -2},
		{-2, -3},
		{-2, -2},
		{-2, -3}
	   },
	/* L */
	  { // 2
		{-2, -3},
		{-2, -3},
		{-2, -3},
		{-2, -2}
	   },
	/* L mirrored */
	  { // 3
		{-2, -3},
		{-2, -2},
		{-2, -3},
		{-2, -3}
	   },
	/* N */
	  { // 4
		{-2, -3},
		{-2, -3},
		{-2, -3},
		{-2, -2}
	   },
	/* N mirrored */
	  { // 5
		{-2, -3},
		{-2, -3},
		{-2, -3},
		{-2, -2}
	   },
	/* T */
	  { // 6
		{-2, -3},
		{-2, -3},
		{-2, -3},
		{-2, -2}
	   },
};

/*
======================================
Return the type of a block (0 = no-block, 1 = normal block, 2 = pivot (rotation) block)

Parameters:

>> pPiece:				 Piece to draw
>> pRotation:			 1 of the 4 possible rotations
>> positionX:            Horizontal position in blocks
>> positionY:            Vertical position in blocks
======================================
*/
int Pieces::getBlockType(unsigned int pPiece, int pRotation, int positionX, int positionY)
{
	return pieceIndexs[pPiece][pRotation][positionX][positionY];
}

/*
======================================
Returns the horizontal displacement of the piece that has to be applied in order to create it in the
correct position.

Parameters:

>> pPiece:    Piece to draw
>> pRotation: 1 of the 4 possible rotations
======================================
*/
int Pieces::getXInitialPosition(int pPiece, int pRotation)
{
	//return pieceIndexsInitialPosition[0][0][0];
	return pieceIndexsInitialPosition[pPiece][pRotation][0];
}

/*
======================================
Returns the vertical displacement of the piece that has to be applied in order to create it in the
correct position.

Parameters:

>> pPiece:    Piece to draw
>> pRotation: 1 of the 4 possible rotations
======================================
*/
int Pieces::getYInitialPosition(int pPiece, int pRotation)
{
	return pieceIndexsInitialPosition[pPiece][pRotation][1];
}