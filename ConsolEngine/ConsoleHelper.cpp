#include "ConsoleHelper.h"
#include "MathHelper.h"

#include <string>
#include <vector>
#include <conio.h>

void ConsoleHelper::preventManualWindowResize()
{
	char windowTitle[256];
	// get handle of currently active window
	HWND windowHandler = GetForegroundWindow();
	// updates wnd_title with title of current window
	GetWindowText(windowHandler, (LPWSTR)windowTitle, sizeof(windowTitle));

	long dwStyle;
	HWND hwndGlut;
	hwndGlut = FindWindow(NULL, (LPWSTR)windowTitle);
	dwStyle = GetWindowLong(hwndGlut, GWL_STYLE);
	// specify window styles
	// ^ = bitwise xor eg 00000101 ^ 00001001 = 00001100 (removed common 1 in the end)
	// | = bitwise or eg 00000101 ^ 00001001 = 00001101  (all 1 merged)
	dwStyle ^= WS_MAXIMIZEBOX | WS_THICKFRAME;
	SetWindowLong(hwndGlut, GWL_STYLE, dwStyle);
}

void ConsoleHelper::resize(HANDLE hConsole, SHORT xSize, SHORT ySize)
{
	// Hold Current Console Buffer Info 
	CONSOLE_SCREEN_BUFFER_INFO consoleBufferInfo;
	// specify rectangle vertices for console window
	// catches input in that game area

	 // Hold the New Console Size 
	SMALL_RECT srWindowRect;
	COORD coordScreen;

	GetConsoleScreenBufferInfo(hConsole, &consoleBufferInfo);

	// Get the Largest Size we can size the Console Window to 
	coordScreen = GetLargestConsoleWindowSize(hConsole);

	// Set internal buffer  size of console window
	// Define the New Console Window Size and Scroll Position 
	srWindowRect.Right = static_cast<SHORT>(min(xSize, coordScreen.X) - 1);
	srWindowRect.Bottom = static_cast<SHORT>(min(ySize, coordScreen.Y)) - 1;
	// intialize 2 variables, one is equal to another with value
	srWindowRect.Left = srWindowRect.Top = static_cast <SHORT>(0);

	// Define the New Console Buffer Size
	// X & Y must be greater than window rectangle from window info
	coordScreen.X = xSize;
	coordScreen.Y = ySize;

	DWORD windowAreaSquare = (DWORD)MathHelper::calculateSquare(consoleBufferInfo.dwSize.X, consoleBufferInfo.dwSize.Y);
	DWORD resizeAreaSquare = (DWORD)MathHelper::calculateSquare(xSize, ySize);

	// If the Current Buffer is Larger than what we want, Resize the 
	// Console Window First, then the Buffer 
	if (windowAreaSquare > resizeAreaSquare)
	{
		ConsoleHelper::validateConsoleResize("SetConsoleWindowInfo_1", SetConsoleWindowInfo(hConsole, TRUE, &srWindowRect));
		ConsoleHelper::validateConsoleResize("SetConsoleScreenBufferSize_1", SetConsoleScreenBufferSize(hConsole, coordScreen));
	}

	// If the Current Buffer is Smaller than what we want, Resize the 
	// Buffer First, then the Console Window 
	if (windowAreaSquare < resizeAreaSquare)
	{
		ConsoleHelper::validateConsoleResize("SetConsoleScreenBufferSize_2", SetConsoleScreenBufferSize(hConsole, coordScreen));
		ConsoleHelper::validateConsoleResize("SetConsoleWindowInfo_2", SetConsoleWindowInfo(hConsole, TRUE, &srWindowRect));
	}

	// If the Current Buffer *is* the Size we want, Don't do anything! 
	return;
}

void ConsoleHelper::setUpFontSettings()
{
	// more info here https://docs.microsoft.com/en-us/windows/console/console-font-infoex
	CONSOLE_FONT_INFOEX consoleFontInfo;
	consoleFontInfo.cbSize = sizeof(consoleFontInfo);
	consoleFontInfo.nFont = 0;
	// Width of each character in the font in pixels
	consoleFontInfo.dwFontSize.X = 0;
	// Height in pixels
	consoleFontInfo.dwFontSize.Y = 22;
	consoleFontInfo.FontFamily = FF_ROMAN;
	consoleFontInfo.FontWeight = FW_BOLD;
	// Choose your font
	_tcscpy_s(consoleFontInfo.FaceName, TEXT("Consolas"));
	SetCurrentConsoleFontEx(GetStdHandle(STD_OUTPUT_HANDLE), FALSE, &consoleFontInfo);
}

void ConsoleHelper::logPressedKeyCode()
{
	int keyCode;
	while ((keyCode = _getch()) != 27) /* 27 = Esc key */
	{
		printf("%d", keyCode);
		printf(", %d", _getch());
		if (keyCode == 0 || keyCode == 224) {
			printf("\n");
		}
	}
	printf("ESC %d\n", keyCode);
}

void ConsoleHelper::validateConsoleResize(const std::string  methodName, const bool isSuccesfull) {
	if (!isSuccesfull)
	{
		std::cout << methodName + " failed with error " << GetLastError() << std::endl;
		exit(0);
	}
}