// #pragma once - replaced here with #ifndef

#ifndef TETRIS_GAME_H
#define TETRIS_GAME_H

#include "BaseApp.h"
#include "Board.h"
#include "MathHelper.h"

/*
it is better practise to use constants as static members
because this way other devs can see where constant comes from, eg Test::CONST_NAME
*/

// game controls, numbers are windows key codes
inline const short int  ROTATE = 32;
inline const short int  TURN_LEFT = 75;
inline const short int  TURN_RIGHT = 77;
// increses speed of current piece
inline const short int  MOVE_DOWN = 80;

inline const short int  WINDOW_WIDTH = 26;
inline const short int  WINDOW_HEIGHT = 20;

class TetrisGame : public BaseApp
{
	// calling Parent we will use BaseApp, eg new Parent() wil lcreate BaseApp(), typedef = new type defenition
	typedef BaseApp Parent;

private:
	// all initial, default values must be initialized to avoid memory, allocations leaks, errors

	// Position of the piece that is falling down
	short int currentPieceX = 0;
	short int currentPieceY = 0;
	// Kind and rotation the piece that is falling down
	short int pieceIndex = 0;
	short int rotationIndex = 0;

	// Position of the next piece
	short int mNextPosX = 0;
	short int mNextPosY = 0;

	// Kind and rotation of the next piece
	short int mNextPiece = 0;
	short int mNextRotation = 0;

	short int totalScores = 0;

	Board *board;
	Pieces *pieces;

	// how many time passed since current piece moved down
	float milesecondsSinceLastMoveDown = 0;
	// how much mileseconds should pass for next piece move down
	float gameSpeedInMileseconds = 1500;

	void initGame();
	void drawText(short int xPosition, const short  int yAxisPosition, std::string text);
	void drawBoard();
	// draws line between score and upper game area with play and next piece areas
	void drawScores();
	// Drawing the blocks that are already stored in the board
	void drawStoredPieces();
	void clearOldPiecePreview();
	void drawPiece(const short int positionX, const short  int positionY, const short int  pPiece, const short int pRotation);


public:
	TetrisGame(Board *board, Pieces *pPieces);

	static const short int  SCORES_PER_LINE = 10;
	static const short int  SPEED_INCREASE_PER_LINE = 100;

	void drawScene();
	void createNewPiece();
	void KeyPressed(const   int btnCode);
	void updateTick(const float deltaTime);
	/*
	Helps manipulate gameSpeed
	*/
	bool isCurrentPieceShouldMoveDown(const float deltaTime);
};
#endif