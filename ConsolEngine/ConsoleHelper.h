#pragma once
#include "TetrisGame.h"
#include <Windows.h>

#include <tchar.h>

class ConsoleHelper
{
private:
	// logs error
	static void validateConsoleResize(const std::string  methodName, const bool isSuccesfull);

public:
	// after calling this, user will not be able edit window
	static void preventManualWindowResize();
	// resize window size
	static void resize(HANDLE hConsole, SHORT xSize, SHORT ySize);
	// configure font
	// we may use many parameters like (font, size, weight...) etc, or use object, eg setUpFontSettings(new Font(font, size, weight...))
	// right now for simplicity we just specify all default, initial values inside method because we are not going to change them during game
	static void setUpFontSettings();
	static void logPressedKeyCode();
};

